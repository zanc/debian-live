Debian Live
===========

This live-build scripts are meant to be used with pbuilder, approx and custom repository served in 127.0.0.1:9997.

Dependencies
------------

Issue the following to install all the required dependencies.

```
# make deps
```

Usage
-----

Pass whatever lb commands to make, for example to build:

```
# make build
```

To clean:

```
# make clean
```

To write the resulting ISO to a usb stick with writable partition, first you need to create one partition for the ISO (`sdX1`) and another one for storage (`sdX2`).

Then issue the following:

```
# dd if=live-image-amd64.hybrid.iso of=/dev/sdX1 bs=1M
```

The following is equivalent to `isohybrid --partok` (install `isolinux` package to provide `isohdppx.bin`):

```
# dd if=/usr/lib/ISOLINUX/isohdppx.bin of=/dev/sdX1
```

Finally, write the MBR to `sdX`, __not sdX1__ (install `syslinux` package to provide `mbr.bin`):

```
# dd if=/usr/lib/SYSLINUX/mbr.bin of=/dev/sdX
```
