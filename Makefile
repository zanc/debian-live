%:
	@lb $@

build:
	@if [ ! -d local/live-build/scripts/build ]; then \
		mkdir -p local/live-build/scripts/build; \
	fi
	@for p in patches/*.patch; do \
		p=$$(readlink -f "$$p"); \
		topatch=$$(basename "$$p" .patch); \
		cd local/live-build/scripts/build; \
			cp -pf /usr/lib/live/build/"$$topatch" .; \
			patch -p1 <"$$p"; \
		cd - >/dev/null; \
	done
	@mkdir -p config/includes.binary
	@cp -pf config/includes.chroot/lib/live/config/1200-local config/includes.binary/configure-etc
	@lb $@

clean:
	@lb $@
	@for p in patches/*.patch; do \
		rm local/live-build/scripts/build/"$$(basename "$$p" .patch)"; \
	done
	@rm -f config/includes.binary/configure-etc

deps:
	@apt-get install live-build live-boot live-config syslinux-common isolinux pxelinux
